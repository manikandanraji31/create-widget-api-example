export const api = (widgetId) => {
  return {
    setLabel(label) {
      window.a8forms.setValue(widgetId, "properties", { label })
    },
  }
}

export const typeDefs = `
  interface ILabelDemo {
    setLabel(label: string) => void;
  }

  LabelDemo(widgetId: string): ILabelDemo;
`
