import React from "react";

const App = (props) => {
  const { properties } = props.renderer ? props : props.json;

  return (
    <div>
      <p>The label for this component: {properties.label}</p>
    </div>
  );
};

export default App;
