import ReactDOM from "react-dom";
import App from "./App";
import { config } from "./config";
import { api, typeDefs } from "./api";

class Component {
  config = config;
  api = api;
  typeDefs = typeDefs;

  render(pluginId, props) {
    const isPresent = document.getElementById(pluginId);

    if (isPresent) {
      ReactDOM.render(<App {...props} />, document.getElementById(pluginId));
    }
  }
  unmount(pluginId) {
    const isPresent = document.getElementById(pluginId);

    if (isPresent) {
      ReactDOM.unmountComponentAtNode(document.getElementById(pluginId));
    }
  }
}

window.LabelDemo = {
  bundleId: "LabelDemo",
  type: "Widget",
  data: new Component()
}

if (document.getElementById("sample-widget-root")) {
  const props = {
    renderer: false,
    properties: {},
    data: {},
    json: {
      properties: {},
    }
  }
  ReactDOM.render(<App {...props}/>, document.getElementById("sample-widget-root"));  
}
